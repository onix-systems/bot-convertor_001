<?php

return [
    'token' => env('SLACK_TOKEN'),
    'bot_id' => env('SLACK_BOT_ID')
];