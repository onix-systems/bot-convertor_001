<?php

return [
    'languages' => [
        'en' => 'Data',
        'ru' => 'Данные'
    ],

    'units' => [
        'bit' => include 'units/bit.php',
        'byte' => include 'units/byte.php',
        'kilobyte' => include 'units/kilobyte.php',
        'megabyte' => include 'units/megabyte.php',
        'gigabyte' => include 'units/gigabyte.php',
        'terabyte' => include 'units/terabyte.php',
        'petabyte' => include 'units/petabyte.php',
        'exabyte' => include 'units/exabyte.php',
        'zettabyte' => include 'units/zettabyte.php',
        'yottabyte' => include 'units/yottabyte.php'
    ]
];