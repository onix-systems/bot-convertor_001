<?php

return [
    'from' => 'bcmul(n, bcpow(2, 33))',
    'to' => 'bcdiv(n, bcpow(2, 33))',
    'languages' => [
        'en' => [
            'name' => 'Gigabyte',
            'aliases' => ['gigabyte', 'gb', 'gigabytes']
        ],
        'ru' => [
            'name' => 'Гигабайт',
            'aliases' => ['гигабайт', 'гб', 'гигабайтах', 'гигабайтов', 'гигабайта']
        ]
    ]
];