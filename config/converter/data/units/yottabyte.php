<?php

return [
    'from' => 'bcmul(n, bcpow(2, 83))',
    'to' => 'bcdiv(n, bcpow(2, 83))',
    'languages' => [
        'en' => [
            'name' => 'Yottabyte',
            'aliases' => ['yottabyte', 'yb', 'yottabytes']
        ],
        'ru' => [
            'name' => 'Иоттабайт',
            'aliases' => ['иоттабайт', 'иб', 'иоттабайтах', 'иоттабайтов', 'иоттабайта']
        ]
    ]
];