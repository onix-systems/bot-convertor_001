<?php

return [
    'from' => 'bcmul(n, bcpow(2, 63))',
    'to' => 'bcdiv(n, bcpow(2, 63))',
    'languages' => [
        'en' => [
            'name' => 'Exabyte',
            'aliases' => ['exabyte', 'eb', 'exabytes']
        ],
        'ru' => [
            'name' => 'Эксабайт',
            'aliases' => ['эксабайт', 'эб', 'эксабайтах', 'эксабайтов', 'эксабайта']
        ]
    ]
];