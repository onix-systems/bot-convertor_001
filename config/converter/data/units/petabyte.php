<?php

return [
    'from' => 'bcmul(n, bcpow(2, 53))',
    'to' => 'bcdiv(n, bcpow(2, 53))',
    'languages' => [
        'en' => [
            'name' => 'Petabyte',
            'aliases' => ['petabyte', 'pb', 'petabytes']
        ],
        'ru' => [
            'name' => 'Петабайт',
            'aliases' => ['петабайт', 'пб', 'петабайтах', 'петабайтов', 'петабайта']
        ]
    ]
];