<?php

return [
    'from' => 'bcmul(n, bcpow(2, 43))',
    'to' => 'bcdiv(n, bcpow(2, 43))',
    'languages' => [
        'en' => [
            'name' => 'Terabyte',
            'aliases' => ['terabyte', 'tb', 'terabytes']
        ],
        'ru' => [
            'name' => 'Терабайт',
            'aliases' => ['терабайт', 'тб', 'терабайтах', 'теребайтов', 'терабайта']
        ]
    ]
];