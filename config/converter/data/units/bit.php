<?php

return [
    'from' => 'n',
    'to' => 'n',
    'languages' => [
        'en' => [
            'name' => 'Bit',
            'aliases' => ['bit', 'bits']
        ],
        'ru' => [
            'name' => 'Бит',
            'aliases' => ['бит', 'битов', 'битах', 'бита']
        ]
    ]
];