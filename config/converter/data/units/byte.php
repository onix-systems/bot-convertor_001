<?php

return [
    'from' => 'bcmul(n, bcpow(2, 3))',
    'to' => 'bcdiv(n, bcpow(2, 3))',
    'languages' => [
        'en' => [
            'name' => 'Byte',
            'aliases' => ['byte', 'bytes']
        ],
        'ru' => [
            'name' => 'Байт',
            'aliases' => ['байт', 'байтов', 'байтах', 'байта']
        ]
    ]
];