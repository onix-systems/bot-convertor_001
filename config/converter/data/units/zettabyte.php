<?php

return [
    'from' => 'bcmul(n, bcpow(2, 73))',
    'to' => 'bcdiv(n, bcpow(2, 73))',
    'languages' => [
        'en' => [
            'name' => 'Zettabyte',
            'aliases' => ['zettabyte', 'zb', 'zettabytes']
        ],
        'ru' => [
            'name' => 'Зеттабайт',
            'aliases' => ['зеттабайт', 'зб', 'зеттабайтах', 'эксабайтов', 'эксабайта']
        ]
    ]
];