<?php

return [
    'from' => 'bcmul(n, bcpow(2, 13))',
    'to' => 'bcdiv(n, bcpow(2, 13))',
    'languages' => [
        'en' => [
            'name' => 'Kilobyte',
            'aliases' => ['kilobyte', 'kb', 'kilobytes']
        ],
        'ru' => [
            'name' => 'Килобайт',
            'aliases' => ['кб', 'килобайтах', 'килобайтов', 'килобайт', 'килобайта']
        ]
    ]
];