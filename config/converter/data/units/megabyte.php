<?php

return [
    'from' => 'bcmul(n, bcpow(2, 23))',
    'to' => 'bcdiv(n, bcpow(2, 23))',
    'languages' => [
        'en' => [
            'name' => 'Megabyte',
            'aliases' => ['megabyte', 'mb', 'megabytes']
        ],
        'ru' => [
            'name' => 'Мегабайт',
            'aliases' => ['мб', 'мегабайтах', 'мегабайтов', 'мегабайт', 'мегабайта']
        ]
    ]
];