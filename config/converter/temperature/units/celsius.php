<?php

return [
    'from' => 'n',
    'to' => 'n',
    'languages' => [
        'en' => [
            'name' => 'Celsius',
            'aliases' => ['c', 'celsius']
        ],
        'ru' => [
            'name' => 'Цельсий',
            'aliases' => ['цельсий', 'цельсия', 'ц', 'цельсиях']
        ]
    ]
];