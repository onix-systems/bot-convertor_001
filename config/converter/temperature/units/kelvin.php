<?php

return [
    'from' => 'bcsub(n, 273.15)',
    'to' => 'bcadd(n, 273.15)',
    'languages' => [
        'en' => [
            'name' => 'Kelvin',
            'aliases' => ['k', 'kelvin']
        ],
        'ru' => [
            'name' => 'Кельвин',
            'aliases' => ['к', 'кельвин', 'кельвина', 'кельвинах']
        ]
    ]
];