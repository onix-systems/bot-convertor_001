<?php

return [
    'from' => 'bcmul(bcsub(n, 32), bcdiv(5, 9))',
    'to' => 'bcadd(bcmul(n, bcdiv(9, 5)), 32)',
    'languages' => [
        'en' => [
            'name' => 'Fahrenheit',
            'aliases' => ['f', 'fahrenheit']
        ],
        'ru' => [
            'name' => 'Фаренгейт',
            'aliases' => ['ф', 'фаренгейт', 'фаренгейта', 'фаренгейтах']
        ]
    ]
];