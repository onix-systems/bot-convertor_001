<?php

return [
    'languages' => [
        'en' => 'Temperature',
        'ru' => 'Температура'
    ],

    'units' => [
        'celsius' => include 'units/celsius.php',
        'fahrenheit' => include 'units/fahrenheit.php',
        'kelvin' => include 'units/kelvin.php',
    ]
];