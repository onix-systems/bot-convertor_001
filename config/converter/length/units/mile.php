<?php

return [
    'from' => 'bcmul(bcmul(n, 1.609344), 1000)',
    'to' => 'bcdiv(bcmul(n, 0.62137119), 1000)',
    'languages' => [
        'en' => [
            'name' => 'Mile',
            'aliases' => ['mi', 'mile', 'miles']
        ],
        'ru' => [
            'name' => 'Миля',
            'aliases' => ['ми', 'миля', 'мили', 'миль'],
        ]
    ]
];