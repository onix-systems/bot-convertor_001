<?php

return [
    'from' => 'bcdiv(bcdiv(n, 0.39370), 100)',
    'to' => 'bcmul(bcmul(n, 0.39370), 100)',
    'languages' => [
        'en' => [
            'name' => 'Inch',
            'aliases' => ['inch', 'inches']
        ],
        'ru' => [
            'name' => 'Дюйм',
            'aliases' => ['дюйм', 'дюйма', 'дюймов', 'дюймах']
        ]
    ]
];