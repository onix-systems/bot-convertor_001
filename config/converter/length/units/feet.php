<?php

return [
    'from' => 'bcdiv(n, 3.28084)',
    'to' => 'bcmul(n, 3.28084)',
    'languages' => [
        'en' => [
            'name' => 'Feet',
            'aliases' => ['ft', 'feet', 'feets', 'fts', 'foot', 'foots']
        ],
        'ru' => [
            'name' => 'Фут',
            'aliases' => ['фт', 'фут', 'фута', 'футах', 'футов']
        ]
    ],
];