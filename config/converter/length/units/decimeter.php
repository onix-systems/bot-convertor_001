<?php

return [
    'from' => 'bcdiv(n, 10)',
    'to' => 'bcmul(n, 10)',
    'languages' => [
        'en' => [
            'name' => 'Decimeter',
            'aliases' => ['dm', 'decimeter', 'decimeters', 'decimetre', 'decimetres']
        ],
        'ru' => [
            'name' => 'Дециметр',
            'aliases' => ['дм', 'дециметр', 'дециметра', 'дециметрах', 'дециметров']
        ]
    ],
];