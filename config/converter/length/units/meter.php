<?php

return [
    'from' => 'n',
    'to' => 'n',
    'languages' => [
        'en' => [
            'name' => 'Meter',
            'aliases' => ['m', 'meter', 'meters', 'metre', 'metres']
        ],
        'ru' => [
            'name' => 'Метр',
            'aliases' => ['м', 'метр', 'метров', 'метрах', 'метра']
        ]
    ]
];