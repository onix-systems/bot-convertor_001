<?php

return [
    'from' => 'bcmul(n, 1000)',
    'to' => 'bcdiv(n, 1000)',
    'languages' => [
        'en' => [
            'name' => 'Kilometer',
            'aliases' => ['km', 'kilometer', 'kilometers', 'kilometre', 'kilometres']
        ],
        'ru' => [
            'name' => 'Километр',
            'aliases' => ['км', 'километр', 'километров', 'километрах', 'километра']
        ]
    ],
];