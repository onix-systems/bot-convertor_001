<?php

return [
    'from' => 'bcdiv(n, 100)',
    'to' => 'bcmul(n, 100)',
    'languages' => [
        'en' => [
            'name' => 'Centimeter',
            'aliases' => ['cm', 'centimeter', 'centimeters', 'centimetre', 'centimetres'],
        ],
        'ru' => [
            'name' => 'Сантиметр',
            'aliases' => ['см', 'сантиметр', 'сантиметрах', 'сантиметров', 'сантиметра',]
        ]
    ]
];