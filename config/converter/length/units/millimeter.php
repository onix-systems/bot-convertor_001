<?php

return [
    'from' => 'bcdiv(n, 1000)',
    'to' => 'bcmul(n, 1000)',
    'languages' => [
        'en' => [
            'name' => 'Millimeter',
            'aliases' => ['mm', 'millimeter', 'millimeters', 'millimetre', 'millimetres'],
        ],
        'ru' => [
            'name' => 'Миллиметр',
            'aliases' => ['мм', 'миллиметра', 'миллиметрах', 'миллиметров', 'миллиметр'],
        ]
    ],
];