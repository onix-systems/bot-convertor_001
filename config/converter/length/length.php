<?php

return [
    'languages' => [
        'en' => 'Length',
        'ru' => 'Длина'
    ],

    'units' => [
        'millimeter' => include 'units/millimeter.php',
        'centimeter' => include 'units/centimeter.php',
        'inch' => include 'units/inch.php',
        'meter' => include 'units/meter.php',
        'decimeter' => include 'units/decimeter.php',
        'kilometer' => include 'units/kilometer.php',
        'mile' => include 'units/mile.php',
        'feet' => include 'units/feet.php',
    ]
];