<?php

return [
    'from' => 'bcmul(n, 31536000)',
    'to' => 'bcdiv(n, 31536000)',
    'languages' => [
        'en' => [
            'name' => 'Year (365)',
            'aliases' => ['y', 'yr', 'ys', 'year', 'years']
        ],
        'ru' => [
            'name' => 'Год (365)',
            'aliases' => ['год', 'года', 'лет', 'годах', 'годы', 'году', 'годе']
        ]
    ]
];