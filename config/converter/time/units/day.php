<?php

return [
    'from' => 'bcmul(n, 86400)',
    'to' => 'bcdiv(n, 86400)',
    'languages' => [
        'en' => [
            'name' => 'Day',
            'aliases' => ['d', 'day', 'days']
        ],
        'ru' => [
            'name' => 'День',
            'aliases' => ['д', 'день', 'дня', 'дней', 'днях', 'дни']
        ]
    ]
];