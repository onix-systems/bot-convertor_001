<?php

return [
    'from' => 'n',
    'to' => 'n',
    'languages' => [
        'en' => [
            'name' => 'Second',
            'aliases' => ['s', 'sec', 'second', 'seconds']
        ],
        'ru' => [
            'name' => 'Секунда',
            'aliases' => ['с', 'секунда', 'секунд', 'секундах', 'секунды']
        ]
    ]
];