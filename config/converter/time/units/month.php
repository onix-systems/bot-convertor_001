<?php

return [
    'from' => 'bcmul(n, 2678400)',
    'to' => 'bcdiv(n, 2678400)',
    'languages' => [
        'en' => [
            'name' => 'Month (31)',
            'aliases' => ['month', 'mon', 'months']
        ],
        'ru' => [
            'name' => 'Месяц (31)',
            'aliases' => ['месяц', 'месяца', 'месяцей', 'месяцах', 'месяцы']
        ]
    ]
];