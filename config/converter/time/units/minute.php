<?php

return [
    'from' => 'bcmul(n, 60)',
    'to' => 'bcdiv(n, 60)',
    'languages' => [
        'en' => [
            'name' => 'Minute',
            'aliases' => ['min', 'minute', 'minutes', 'mins']
        ],
        'ru' => [
            'name' => 'Минута',
            'aliases' => ['мин', 'минут', 'минута', 'минутах', 'минуты']
        ]
    ]
];