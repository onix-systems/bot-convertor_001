<?php

return [
    'from' => 'bcdiv(n, 1000)',
    'to' => 'bcmul(n, 1000)',
    'languages' => [
        'en' => [
            'name' => 'Millisecond',
            'aliases' => ['ms', 'millisecond', 'milliseconds']
        ],
        'ru' => [
            'name' => 'Миллисекунда',
            'aliases' => ['мс', 'миллисекунда', 'миллисекунд', 'миллисекундах', 'миллисекунды']
        ]
    ]
];