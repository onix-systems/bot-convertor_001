<?php

return [
    'from' => 'bcmul(n, 604800)',
    'to' => 'bcdiv(n, 604800)',
    'languages' => [
        'en' => [
            'name' => 'Week',
            'aliases' => ['w', 'week', 'weeks']
        ],
        'ru' => [
            'name' => 'Неделя',
            'aliases' => ['неделя', 'недель', 'недели', 'неделях']
        ]
    ]
];