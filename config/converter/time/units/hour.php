<?php

return [
    'from' => 'bcmul(n, 3600)',
    'to' => 'bcdiv(n, 3600)',
    'languages' => [
        'en' => [
            'name' => 'Hour',
            'aliases' => ['h', 'hour', 'hours']
        ],
        'ru' => [
            'name' => 'Час',
            'aliases' => ['ч', 'час', 'часа', 'часов', 'часах', 'часы']
        ]
    ]
];