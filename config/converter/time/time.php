<?php

return [
    'languages' => [
        'en' => 'Time',
        'ru' => 'Время'
    ],

    'units' => [
        'millisecond' => include 'units/millisecond.php',
        'second' => include 'units/second.php',
        'minute' => include 'units/minute.php',
        'hour' => include 'units/hour.php',
        'day' => include 'units/day.php',
        'week' => include 'units/week.php',
        'month' => include 'units/month.php',
        'year' => include 'units/year.php'
    ]
];