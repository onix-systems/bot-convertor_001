<?php

return [
    'from' => 'bcdiv(n, 1000)',
    'to' => 'bcmul(n, 1000)',
    'languages' => [
        'en' => [
            'name' => 'Milligram',
            'aliases' => ['mg', 'milligram', 'milligrams', 'milligramme', 'milligrammes']
        ],
        'ru' => [
            'name' => 'Миллиграмм',
            'aliases' => ['мг', 'миллиграмма', 'миллиграмм', 'миллиграммов', 'миллиграммах']
        ]
    ]
];