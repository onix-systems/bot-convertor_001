<?php

return [
    'from' => 'bcmul(n, 100000)',
    'to' => 'bcdiv(n, 100000)',
    'languages' => [
        'en' => [
            'name' => 'Centner',
            'aliases' => ['ct', 'cn', 'centner', 'centners', 'quintal', 'quintals']
        ],
        'ru' => [
            'name' => 'Центнер',
            'aliases' => ['цт', 'центнера', 'центнеров', 'центнерах', 'центнер']
        ]
    ]
];