<?php

return [
    'from' => 'bcmul(n, 1000)',
    'to' => 'bcdiv(n, 1000)',
    'languages' => [
        'en' => [
            'name' => 'Kilogram',
            'aliases' => ['kg', 'kgs', 'kilogram', 'kilograms', 'kilogramme', 'kilogrammes']
        ],
        'ru' => [
            'name' => 'Килограмм',
            'aliases' => ['кг', 'килограмм', 'килограмма', 'килограммов', 'килограммах']
        ]
    ]
];