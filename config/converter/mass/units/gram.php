<?php

return [
    'from' => 'n',
    'to' => 'n',
    'languages' => [
        'en' => [
            'name' => 'Gram',
            'aliases' => ['g', 'gs', 'gram', 'grams', 'gramme', 'grammes']
        ],
        'ru' => [
            'name' => 'Грамм',
            'aliases' => ['г', 'грамм', 'грамма', 'граммах', 'гарммов']
        ]
    ],
];