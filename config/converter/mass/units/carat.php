<?php

return [
    'from' => 'bcdiv(n, 5)',
    'to' => 'bcmul(n, 5)',
    'languages' => [
        'en' => [
            'name' => 'Carat',
            'aliases' => ['ct', 'cr', 'carat', 'carats']
        ],
        'ru' => [
            'name' => 'Карат',
            'aliases' => ['кр', 'кт', 'карат', 'карата', 'каратах', 'каратах']
        ]
    ]
];