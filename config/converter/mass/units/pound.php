<?php

return [
    'from' => 'bcdiv(n, 0.0022046)',
    'to' => 'bcmul(n, 0.0022046)',
    'languages' => [
        'en' => [
            'name' => 'Pound',
            'aliases' => ['lb', 'pound', 'pounds', 'lbs']
        ],
        'ru' => [
            'name' => 'Фунт',
            'aliases' => ['фунт', 'фунта', 'фунтов', 'фунтах']
        ]
    ]
];