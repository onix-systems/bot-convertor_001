<?php

return [
    'from' => 'bcmul(n, 1000000)',
    'to' => 'bcdiv(n, 1000000)',
    'languages' => [
        'en' => [
            'name' => 'Tonne',
            'aliases' => ['t', 'tn', 'ton', 'tons', 'tonne', 'tonnes']
        ],
        'ru' => [
            'name' => 'Тонна',
            'aliases' => ['т', 'тонн', 'тоннах', 'тонны']
        ]
    ]
];