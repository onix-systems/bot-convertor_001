<?php

return [
    'languages' => [
        'en' => 'Mass',
        'ru' => 'Масса'
    ],

    'units' => [
        'milligram' => include 'units/milligram.php',
        'carat' => include 'units/carat.php',
        'gram' => include 'units/gram.php',
        'kilogram' => include 'units/kilogram.php',
        'pound' => include 'units/pound.php',
        'centner' => include 'units/centner.php',
        'tonne' => include 'units/tonne.php'
    ]
];