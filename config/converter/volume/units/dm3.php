<?php

return [
    'from' => 'n',
    'to' => 'n',
    'languages' => [
        'en' => [
            'name' => 'Cubic Decimeter',
            'aliases' => ['cdm', 'dm3']
        ],
        'ru' => [
            'name' => 'Кубический дециметр',
            'aliases' => ['кдм', 'дм3']
        ]
    ]
];