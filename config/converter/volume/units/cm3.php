<?php

return [
    'from' => 'bcdiv(n, 1000)',
    'to' => 'bcmul(n, 1000)',
    'languages' => [
        'en' => [
            'name' => 'Cubic centimeter',
            'aliases' => ['cc', 'cm3', 'cc', 'cbcm', 'ccm']
        ],
        'ru' => [
            'name' => 'Кубический сантиметр',
            'aliases' => ['ксм', 'см3']
        ]
    ]
];