<?php

return [
    'from' => 'bcdiv(n, 0.0062898)',
    'to' => 'bcmul(n, 0.0062898)',
    'languages' => [
        'en' => [
            'name' => 'Barrel',
            'aliases' => ['b', 'barrel', 'barrels']
        ],
        'ru' => [
            'name' => 'Баррель',
            'aliases' => ['б', 'баррель', 'барреля', 'баррелях', 'баррелей', 'баррели']
        ]
    ]
];