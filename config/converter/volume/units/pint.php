<?php

return [
    'from' => 'bcdiv(n, 2.1134)',
    'to' => 'bcmul(n, 2.1134)',
    'languages' => [
        'en' => [
            'name' => 'Pint',
            'aliases' => ['pint', 'pints']
        ],
        'ru' => [
            'name' => 'Пинта',
            'aliases' => ['пт', 'пинт', 'пинта', 'питнах', 'пинты']
        ]
    ]
];