<?php

return [
    'from' => 'bcmul(n, 1000000000000)',
    'to' => 'bcdiv(n, 1000000000000)',
    'languages' => [
        'en' => [
            'name' => 'Cubic kilometer',
            'aliases' => ['cbkm', 'km3']
        ],
        'ru' => [
            'name' => 'Кубический километр',
            'aliases' => ['ккм', 'км3']
        ]
    ]
];