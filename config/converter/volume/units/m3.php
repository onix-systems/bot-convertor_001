<?php

return [
    'from' => 'bcmul(n, 1000)',
    'to' => 'bcdiv(n, 1000)',
    'languages' => [
        'en' => [
            'name' => 'Cubic meter',
            'aliases' => ['cbm', 'm3']
        ],
        'ru' => [
            'name' => 'Кубический метр',
            'aliases' => ['м3']
        ]
    ]
];