<?php

return [
    'from' => 'n',
    'to' => 'n',
    'languages' => [
        'en' => [
            'name' => 'Liter',
            'aliases' => ['l', 'liter', 'liters', 'litre', 'litres']
        ],
        'ru' => [
            'name' => 'Литр',
            'aliases' => ['л', 'литр', 'литра', 'литров', 'литрах', 'литры']
        ]
    ]
];