<?php

return [
    'from' => 'bcdiv(n, 1000000)',
    'to' => 'bcmul(n, 1000000)',
    'languages' => [
        'en' => [
            'name' => 'Cubic millimeter',
            'aliases' => ['cmm', 'mm3']
        ],
        'ru' => [
            'name' => 'Кубический миллиметр',
            'aliases' => ['кмм', 'мм3']
        ]
    ]
];