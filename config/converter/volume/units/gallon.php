<?php

return [
    'from' => 'bcdiv(n, 0.26417)',
    'to' => 'bcmul(n, 0.26417)',
    'languages' => [
        'en' => [
            'name' => 'Gallon',
            'aliases' => ['gal', 'gallon', 'gallons']
        ],
        'ru' => [
            'name' => 'Галлон',
            'aliases' => ['гл', 'галлон', 'галлона', 'галлонах', 'галлоны']
        ]
    ]
];