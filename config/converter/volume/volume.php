<?php

return [
    'languages' => [
        'en' => 'Volume',
        'ru' => 'Объем'
    ],

    'units' => [
        'mm3' => include 'units/mm3.php',
        'cm3' => include 'units/cm3.php',
        'm3' => include 'units/m3.php',
        'dm3' => include 'units/dm3.php',
        'liter' => include 'units/liter.php',
        'km3' => include 'units/km3.php',
        'gallon' => include 'units/gallon.php',
        'pint' => include 'units/pint.php',
        'barrel' => include 'units/barrel.php'
    ]
];