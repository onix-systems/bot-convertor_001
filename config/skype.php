<?php

return [
    'client_id' => env('SKYPE_BOT_CLIENT_ID'),
    'client_secret' => env('SKYPE_BOT_CLIENT_SECRET')
];