<?php

return [
    'default_language' => env('BOT_DEFAULT_LANGUAGE', 'en'),
    'languages' => [
        'en' => [
            'flag' => '🇺🇸',
            'commands' => [
                'start' => [
                    'To get start info in another language, type:',
                    '/start language',
                    'Example:',
                    '/start ru',
                    '',
                    'To get help, type:',
                    '/help language',
                    'Example:',
                    '/help ru',
                    '',
                    'Languages:',
                    ''
                ],

                'help' => [
                    'To see examples of usage, type:',
                    '/examples',
                    '',
                    'Measurement units are divided into categories.',
                    'To see all categories, type:',
                    '/categories',
                    '',
                    'To see all measurement units of category, type:',
                    '/units category name',
                    'Example:',
                    '/units temperature',
                    '',
                    'For all commands you can specify language as last parameter.',
                    'Examples:',
                    '/help ru',
                    '/units temperature ru',
                    '/examples ru',
                    '',
                    'Languages:',
                    ''
                ],

                'examples' => [
                    'All units are case insensitive.',
                    'There are aliases like:',
                    'megabyte = mb',
                    '',
                    'To convert you can type something like:',
                    '24 kb mb',
                    '',
                    'Or',
                    '-48 celsius to f',
                    '(here f = fahrenheit)',
                    '',
                    'Or even easier',
                    '12.5 c f',
                    '',
                    'To see all results type:',
                    '4 gigabytes'
                ],
            ],
        ],
        'ru' => [
            'flag' => '🇷🇺',
            'commands' => [
                'start' => [
                    'Чтобы получить стартовую информацию на другом языке, напишите:',
                    '/start язык',
                    'Например:',
                    '/start en',
                    '',
                    'Чтобы получить помощь, напишите:',
                    '/help язык',
                    'Например:',
                    '/help en',
                    '',
                    'Языки:',
                    ''
                ],

                'help' => [
                    'Чтобы посмотреть примеры использования, напишите:',
                    '/examples',
                    '',
                    'Меры измерения поделены на категории.',
                    'Чтобы посмотреть список категорий, напишите:',
                    '/categories',
                    '',
                    'Чтобы посмотреть список мер измерения интересующей Вас категории, напишите:',
                    '/units название категории',
                    'Например:',
                    '/units temperature',
                    '',
                    'Для всех команд последним параметром можно указать язык.',
                    'Например:',
                    '/help en',
                    '/units temperature en',
                    '/examples en',
                    '',
                    'Языки:',
                    ''
                ],

                'examples' => [
                    'Все меры измерения регистронезависимы.',
                    'Также есть сокращения вроде:',
                    'мегабайт = мб',
                    '',
                    'Чтобы конвертировать, напишите что-то вроде:',
                    '24 кб мб',
                    '',
                    'Или',
                    '-48 цельсий в ф',
                    '(здесь ф = фарангейтах)',
                    '',
                    'Или даже проще',
                    '12.5 ц ф',
                    '',
                    'Можно так же увидеть все результаты просто написав:',
                    '4 гигабайта'
                ],
            ]
        ]
    ]
];