<?php

return [
    'default_language' => env('CONVERTER_DEFAULT_LANGUAGE', 'en'),
    'scale' => env('CONVERTER_SCALE', 50),
    'ignore' => ['to', 'in', 'в'],
    'errors' => [
        'ru' => [
            1 => 'Меры измерения не найдены.',
            2 => 'Значение не найдено.',
            3 => 'Не могу конвертировать эти меры измерения.'
        ],
        'en' => [
            1 => "Can't find measurement units.",
            2 => "Can't find value.",
            3 => "Can't convert these units."
        ]
    ],
    'categories' => [
        'data' => include 'converter/data/data.php',
        'temperature' => include 'converter/temperature/temperature.php',
        'length' => include 'converter/length/length.php',
        'mass' => include 'converter/mass/mass.php',
        'time' => include 'converter/time/time.php',
        'volume' => include 'converter/volume/volume.php',
    ]
];