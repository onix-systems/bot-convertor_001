<?php

namespace App\Services;

use App\Facades\ConverterFacade;

class Bot
{
    protected $botConfig;
    protected $converterConfig;

    public function __construct($botConfig, $converterConfig)
    {
        $this->botConfig = $botConfig;
        $this->converterConfig = $converterConfig;
        $this->language = $botConfig['default_language'];
    }

    public function start($arguments = '')
    {
        $arguments = mb_strtolower($arguments);
        $this->setLanguage($arguments);
        $text = $this->getCommandText(__FUNCTION__);
        return $text . $this->getLanguages();
    }

    public function help($arguments = '')
    {
        $arguments = mb_strtolower($arguments);
        $this->setLanguage($arguments);
        $text = $this->getCommandText(__FUNCTION__);
        return $text . $this->getLanguages();
    }

    public function categories($arguments = '')
    {
        $arguments = mb_strtolower($arguments);
        $this->setLanguage($arguments);
        $text = $this->getCommandText(__FUNCTION__);
        return $text . $this->getCategories();
    }

    public function units($arguments = '')
    {
        $arguments = mb_strtolower($arguments);
        $args = explode(' ', $arguments);
        $argumentsCount = count($args);
        if ($argumentsCount && $argumentsCount <= 2) {
            $category = $args[0];
            $language = $argumentsCount === 2 ? $args[1] : '';

            $this->setLanguage($language);
            $units = $this->getUnits($category);
            if ($units) {
                $text = $this->getCommandText(__FUNCTION__);
                return $text . $units;
            }
        }

        return $this->help($arguments);
    }

    public function examples($arguments = '')
    {
        $arguments = mb_strtolower($arguments);
        $this->setLanguage($arguments);
        return $this->getCommandText(__FUNCTION__);
    }

    public function convert($text)
    {
        $result = ConverterFacade::convert($text);
        return $result !== null
            ? $result
            : ConverterFacade::getError();
    }

    protected function getUnits($category)
    {
        $categories = array_keys($this->converterConfig['categories']);
        if (in_array($category, $categories, true)) {
            $units = array_keys($this->converterConfig['categories'][$category]['units']);
            usort($units, function ($a, $b) use ($category) {
                return strnatcmp(
                    $this->converterConfig['categories'][$category]['units'][$a]['languages'][$this->language]['name'],
                    $this->converterConfig['categories'][$category]['units'][$b]['languages'][$this->language]['name']
                );
            });

            $units = array_map(
                function ($unit) use ($category) {
                    return $this->converterConfig['categories'][$category]['units'][$unit]['languages'][$this->language]['name'];
                },
                $units
            );

            return implode(PHP_EOL, $units);
        }

        return [];
    }

    protected function setLanguage($language)
    {
        if (in_array($language, array_keys($this->botConfig['languages']), true)) {
            $this->language = $language;
        }
    }

    protected function getLanguages()
    {
        $languages = array_keys($this->botConfig['languages']);
        $languages = array_map(function ($value) {
            return $this->botConfig['languages'][$value]['flag'] . ' ' . $value;
        }, $languages);
        $languages = implode(',  ', $languages);

        return $languages;
    }

    protected function getCategories()
    {
        $categories = array_keys($this->converterConfig['categories']);
        usort($categories, function ($a, $b) {
            return strnatcmp(
                $this->converterConfig['categories'][$a]['languages'][$this->language],
                $this->converterConfig['categories'][$b]['languages'][$this->language]
            );
        });

        $categories = array_map(
            function ($value) {
                return $this->converterConfig['categories'][$value]['languages'][$this->language] .
                ' /units ' . $value;
            },
            $categories
        );

        return implode(PHP_EOL, $categories);
    }

    protected function getCommandText($name)
    {
        $text = &$this->botConfig['languages'][$this->language]['commands'][$name];
        return isset($text) && is_array($text)
            ? implode(PHP_EOL, array_map(function ($value) {
                return htmlentities($value);
            }, $text))
            : '';
    }
}