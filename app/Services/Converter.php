<?php

namespace App\Services;

class Converter
{
    const ERROR_UNITS_NOT_FOUND = 1;
    const ERROR_VALUE_NOT_FOUND = 2;
    const ERROR_CONVERSION = 3;

    protected $units = [];
    protected $aliases = [];
    protected $categories = [];
    protected $ignore = [];
    protected $error = '';
    protected $errors = [];
    protected $minValue;

    public function __construct($config)
    {
        $this->units = $config['categories'];
        $this->ignore = $config['ignore'];
        $this->errors = $config['errors'];
        $this->language = $config['default_language'];
        $this->mapAliases();
        $this->minValue = bcpow(bcdiv(1, 10), $config['scale']);
        bcscale($config['scale']);
    }

    public function convert($expression)
    {
        return $this->analyse($expression);
    }

    public function getError()
    {
        return $this->error;
    }

    protected function error($errorCode)
    {
        $this->error = $this->errors[$this->language][$errorCode];
        return null;
    }

    protected function analyse($expression)
    {
        $value = $this->getValue($expression);

        $expression = $this->cleanExpression($expression, $value);
        $words = $this->splitExpression($expression);
        $aliases = $this->fetchAliases($words);
        $aliasesCount = count($aliases);

        if ($value === null || !$aliasesCount) {
            if ($aliasesCount) {
                $this->language = $this->aliases[$aliases[0]]['language'];
                return $this->error(self::ERROR_VALUE_NOT_FOUND);
            } else {
                return $this->error(self::ERROR_UNITS_NOT_FOUND);
            }
        }

        return $aliasesCount === 2
            ? $this->transform($value, $aliases[0], $aliases[1])
            : $this->transformAll($value, $aliases[0]);
    }

    protected function getValue($expression)
    {
        return preg_match('/^\s*([+-]?(?(?=\.)\.\d+|\d+(?:[\.,]?\d+)?))/', $expression, $matches)
            ? str_replace(',', '.', $matches[1])
            : null;
    }

    protected function fetchAliases($words)
    {
        $units = [];
        while (count($words) && count($units) < 2) {
            $tmp = array_shift($words);
            if ($this->isAlias($tmp)) {
                $units[] = $tmp;
            }
        }

        return $units;
    }

    protected function isAlias($word)
    {
        return in_array($word, $this->getAliases(), true);
    }

    protected function calculate($value, $fromExpression, $toExpression)
    {
        return $this->evaluate($this->evaluate($value, $fromExpression), $toExpression);
    }

    protected function transform($value, $from, $to)
    {
        $fromCategory = $this->aliases[$from]['category'];
        $toCategory = $this->aliases[$to]['category'];
        $this->language = $this->aliases[$from]['language'];
        if ($fromCategory === $toCategory) {
            $fromExpression = $this->aliases[$from]['from'];
            $toExpression = $this->aliases[$to]['to'];
            $toName = $this->aliases[$to]['name'];
            $fromName = $this->aliases[$from]['name'];

            return [
                $fromName => [
                    $toName => $this->beautify(
                        $this->calculate($value, $fromExpression, $toExpression)
                    )
                ]
            ];
        }

        return $this->error(self::ERROR_CONVERSION);
    }

    protected function transformAll($value, $from)
    {
        $fromUnit = $this->aliases[$from]['unit'];
        $fromCategory = $this->aliases[$from]['category'];
        $fromLanguage = $this->aliases[$from]['language'];
        $fromExpression = $this->aliases[$from]['from'];
        $fromName = $this->aliases[$from]['name'];

        $toUnits = $this->categories[$fromCategory];
        usort($toUnits, function ($a, $b) use ($fromCategory, $fromLanguage) {
            return strnatcmp(
                $this->units[$fromCategory]['units'][$a]['languages'][$fromLanguage]['name'],
                $this->units[$fromCategory]['units'][$b]['languages'][$fromLanguage]['name']
            );
        });

        $result = [$fromName => []];
        foreach ($toUnits as $toUnit) {
            if ($toUnit !== $fromUnit) {
                $toName = $this->units[$fromCategory]['units'][$toUnit]['languages'][$fromLanguage]['name'];
                $toExpression = $this->units[$fromCategory]['units'][$toUnit]['to'];

                $tmp = $this->calculate($value, $fromExpression, $toExpression);

                // Exclude abs(number) less than 0.0001
                $abs = starts_with($tmp, '-') ? substr($tmp, 1) : $tmp;
                if ($this->minValue >= 1 || bccomp($this->minValue, $abs) <= 0) {
                    $result[$fromName][$toName] = $this->beautify(
                        $tmp
                    );
                }
            }
        }

        return $result;
    }

    protected function evaluate($value, $expression)
    {
        return eval('return ' . $this->replaceVariable($value, $expression) . ';');
    }

    protected function replaceVariable($value, $expression)
    {
        return str_replace('n', '"' . $value . '"', $expression);
    }

    protected function beautify($value)
    {
        return $this->chunkify($this->trimZeroes($value));
    }

    protected function trimZeroes($value)
    {
        return strrpos($value, '.') !== false
            ? rtrim(rtrim($value, '0'), '.')
            : $value;
    }

    protected function chunkify($value, $chunkSize = 3, $delimiter = ',')
    {
        // If number < 0, remove a minus sign
        // cause it'll be chunkified as -,125.00
        if (!~bccomp($value, 0)) {
            $sign = $value[0];
            $value = substr($value, 1);
        } else {
            $sign = '';
        }

        // Round
        $value = $this->trimZeroes($this->bcround($value, 15));

        // We need to chunkify only whole part of a number
        $parts = explode('.', $value);
        $whole = $parts[0];
        $fraction = isset($parts[1]) ? '.' . $parts[1] : '';

        // Chunkify
        $result = substr(strrev(chunk_split(strrev($whole), $chunkSize, $delimiter)), 1);

        return $sign . $result . $fraction;
    }

    // http://php.net/manual/ru/function.bcscale.php#79628
    protected function bcround($number, $scale = 0)
    {
        $fix = "5";
        for ($i = 0; $i < $scale; $i++) {
            $fix = "0$fix";
        }
        $number = bcadd($number, "0.$fix", $scale + 1);
        return bcdiv($number, "1.0", $scale);
    }

    protected static function cleanExpression($expression, $value)
    {
        return mb_strtolower(
            trim(
                preg_replace('/[^\s0-9A-ZА-ЯЁ]/', '', mb_strtoupper(str_replace($value, '', $expression)))
            )
        );
    }

    protected function splitExpression($expression)
    {
        return array_filter(preg_split('/\s+/', $expression), function ($word) {
            return !empty($word) && !in_array($word, $this->ignore, true);
        });
    }

    protected function mapAliases()
    {
        foreach ($this->units as $category => $categoryData) {
            foreach ($categoryData['units'] as $unit => $properties) {
                $this->categories[$category][] = $unit;
                foreach ($properties['languages'] as $language => $info) {
                    foreach ($info['aliases'] as $alias) {
                        $this->aliases[$alias] = [
                            'unit' => $unit,
                            'category' => $category,
                            'language' => $language,
                            'from' => $properties['from'],
                            'to' => $properties['to'],
                            'name' => $info['name']
                        ];
                    }
                }
            }
        }
    }

    protected function getAliases()
    {
        return array_keys($this->aliases);
    }
}