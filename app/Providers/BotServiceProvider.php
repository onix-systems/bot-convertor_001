<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Bot;

class BotServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('bot', function () {
            return new Bot(
                config('bot'),
                config('converter')
            );
        });
    }
}
