<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Services\Converter;

class ConverterServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('converter', function() {
            return new Converter(
                config('converter')
            );
        });
    }
}
