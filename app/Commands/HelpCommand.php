<?php

namespace App\Commands;

use Telegram\Bot\Commands\Command;
use App\Facades\Bot;

class HelpCommand extends Command
{
    protected $name = 'help';
    protected $description = 'Get help.';

    public function handle($arguments)
    {
        $this->replyWithMessage([
            'text' => Bot::help($arguments),
            'parse_mode' => 'HTML'
        ]);
    }
}
