<?php

namespace App\Commands;

use Telegram\Bot\Commands\Command;
use App\Facades\Bot;

class ExamplesCommand extends Command
{
    protected $name = 'examples';
    protected $description = 'Get examples.';

    public function handle($arguments)
    {
        $this->replyWithMessage([
            'text' => Bot::examples($arguments),
            'parse_mode' => 'HTML'
        ]);
    }
}
