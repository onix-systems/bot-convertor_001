<?php

namespace App\Commands;

use Telegram\Bot\Commands\Command;
use App\Facades\Bot;

class UnitsCommand extends Command
{
    protected $name = 'units';
    protected $description = 'Get list of units for category.';

    public function handle($arguments)
    {
        $this->replyWithMessage([
            'text' => Bot::units($arguments),
            'parse_mode' => 'HTML'
        ]);
    }
}
