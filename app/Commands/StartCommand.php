<?php

namespace App\Commands;

use Telegram\Bot\Commands\Command;
use App\Facades\Bot;

class StartCommand extends Command
{
    protected $name = 'start';
    protected $description = 'Get info about bot.';

    public function handle($arguments)
    {
        $this->replyWithMessage([
            'text' => Bot::start($arguments),
            'parse_mode' => 'HTML'
        ]);
    }
}
