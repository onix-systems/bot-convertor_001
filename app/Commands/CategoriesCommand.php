<?php

namespace App\Commands;

use Telegram\Bot\Commands\Command;
use App\Facades\Bot;

class CategoriesCommand extends Command
{
    protected $name = 'categories';
    protected $description = 'Get all categories.';

    public function handle($arguments)
    {
        $this->replyWithMessage([
            'text' => Bot::categories($arguments),
            'parse_mode' => 'HTML'
        ]);
    }
}
