<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class ConverterFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'converter';
    }
}
