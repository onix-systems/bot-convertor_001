<?php

namespace App\Http\Controllers;

use App\Facades\Bot;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client;

class FacebookController extends Controller
{
    const MAX_RESPONSE_TEXT_LENGTH = 320;

    public function verify(request $request)
    {
        if ($request->query('hub_verify_token') === config('facebook.bot_token')) {
            return $request->query('hub_challenge');
        }
        return 'Error, wrong validation token';
    }

    public function main(Request $request)
    {
        $json = $request->json()->all();
        $messaging = $json['entry'][0]['messaging'];
        foreach ($messaging as $event) {
            $sender = $event['sender']['id'];
            if (isset($event['message']) && isset($event['message']['text'])) {
                $text = trim($event['message']['text']);
                Log::info($text);
                if (starts_with($text, '/')) {
                    $parts = explode(' ', ltrim($text, '/'));
                    $command = $parts[0];
                    $args = implode(' ', array_slice($parts, 1));
                    $response = $this->callBotCommand(mb_strtolower($command), $args);
                } else {
                    $response = $this->formatResponse(Bot::convert($text));
                }

                Log::info($response);
                if ($response) {
                    foreach ($this->splitForMessages($response) as $text) {
                        $this->sendResponse($sender, $text);
                    }
                }
            }
        }
    }

    protected function splitForMessages($text)
    {
        try {
            $pieces = explode(PHP_EOL, $text);
            $result = [];
            $tmp = [];
            foreach ($pieces as $piece) {
                $t = $tmp;
                $t[] = $piece;
                $len = mb_strlen(implode(PHP_EOL, $t));
                if ($len <= self::MAX_RESPONSE_TEXT_LENGTH) {
                    $tmp[] = $piece;
                } else {
                    $result[] = implode(PHP_EOL, $tmp);
                    $tmp = [];
                }
            }

            if (!empty($tmp)) {
                $result[] = implode(PHP_EOL, $tmp);
            }

            return $result;

        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return '';
        }
    }

    protected function callBotCommand($command, $params = '')
    {
        try {
            return $this->callStatic($command, $params);
        } catch (\Exception $e) {
            try {
                return $this->callStatic('help', $params);
            } catch (\Exception $e) {
                return false;
            }
        }
    }

    protected function callStatic($command, $params)
    {
        return forward_static_call(
            [Bot::class, $command],
            $params
        );
    }

    protected function sendResponse($senderId, $text)
    {
        Log::info('RESULT RESPONSE TEXT: ' . $text);
        $client = new Client;
        $client->request('post', 'https://graph.facebook.com/v2.6/me/messages', [
            'synchronous' => true,
            'query' => ['access_token' => config('facebook.bot_token')],
            'json' => ['recipient' => ['id' => $senderId], 'message' => ['text' => $text]]
        ]);
    }

    protected function formatResponse($response)
    {
        $text = $response;
        if (is_array($response)) {
            $text = '';
            foreach ($response as $to) {
                foreach ($to as $toName => $toValue) {
                    $text .= $toName . ' ' . $toValue . PHP_EOL;
                }
            }
        }
        return $text;
    }
}
