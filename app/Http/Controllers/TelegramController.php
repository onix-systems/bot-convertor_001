<?php

namespace App\Http\Controllers;

use Telegram\Bot\Laravel\Facades\Telegram;
use App\Facades\Bot;

class TelegramController extends Controller
{
    const REQUEST_TYPE_MESSAGE = 'message';
    const REQUEST_TYPE_INLINE_QUERY = 'inlineQuery';

    public function main()
    {
        $update = Telegram::getWebhookUpdates();
        switch (true) {
            case $query = $update->getMessage():
                $text = $query->getText();
                $responseId = $query->getChat()->getId();
                $type = self::REQUEST_TYPE_MESSAGE;
                break;

            case $query = $update->getInlineQuery():
                $text = $query->getQuery();
                $responseId = $query->getId();
                $type = self::REQUEST_TYPE_INLINE_QUERY;
                break;

            default:
                return;
        }

        $this->{'answer' . ucfirst($type)}(Bot::convert($text), $text, $responseId);
    }

    protected function makeArticle($title, $message, $parseMode = 'HTML')
    {
        return [
            'type' => 'article',
            'id' => md5($message),
            'title' => $title,
            'message_text' => $message,
            'parse_mode' => $parseMode
        ];
    }

    protected function answerInlineQuery($response, $originalText, $queryId)
    {
        $results = [];
        if (!is_array($response)) {
            $results[] = $this->makeArticle(
                $response,
                $originalText . ' (' . $response . ')'
            );
        } else {
            foreach ($response as $fromName => $to) {
                foreach ($to as $toName => $toValue) {
                    $results[] = $this->makeArticle(
                        $toName . ' ' . $toValue,
                        '<code>' . $toName . '</code> ' . $toValue . ' (' . $originalText . ')'
                    );
                }
            }
        }

        Telegram::answerInlineQuery([
            'inline_query_id' => $queryId,
            'results' => $results
        ]);
    }

    protected function answerMessage($response, $originalText, $chatId)
    {
        if ($originalText[0] !== '/') {
            $text = $response;
            if (is_array($response)) {
                $text = '';
                foreach ($response as $to) {
                    foreach ($to as $toName => $toValue) {
                        $text .= '<code>' . $toName . '</code> ' . $toValue . PHP_EOL;
                    }
                }
            }

            Telegram::sendMessage([
                'chat_id' => $chatId,
                'text' => $text,
                'parse_mode' => 'HTML'
            ]);
        }

        Telegram::commandsHandler(true);
    }
}