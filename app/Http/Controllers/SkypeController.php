<?php

namespace App\Http\Controllers;

use App\Facades\Bot;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class SkypeController extends Controller
{

    public function main(Request $request)
    {
        $text = $request->input('text');
        if ($text === 'ping') {
            return 'OK';
        }

        $conversationId = $request->input('conversation.id');

        if (starts_with($text, '_')) {
            $parts = explode(' ', ltrim($text, '_'));
            $command = $parts[0];
            $args = implode(' ', array_slice($parts, 1));
            self::sendResponse(
                $conversationId,
                self::callBotCommand(mb_strtolower($command), $args)
            );
        } else {
            self::sendResponse(
                $conversationId,
                self::formatResponse(Bot::convert($text))
            );
        }
    }

    private static function sendResponse($conversationId, $text)
    {
        (new Client)
            ->post(
                "https://apis.skype.com/v3/conversations/$conversationId/activities", [
                    'headers' => [
                        'Authorization' => "Bearer " . self::getToken()
                    ],
                    'json' => [
                        'type' => 'message/text',
                        'text' => $text
                    ]
                ]
            );
    }

    private static function getToken()
    {
        return json_decode(
            (new Client)
                ->post(
                    'https://login.microsoftonline.com/common/oauth2/v2.0/token', [
                        'form_params' => [
                            'client_id' => config('skype.client_id'),
                            'client_secret' => config('skype.client_secret'),
                            'grant_type' => 'client_credentials',
                            'scope' => 'https://graph.microsoft.com/.default'
                        ]
                    ]
                )
                ->getBody()
                ->getContents()
        )->access_token;
    }

    private static function callBotCommand($command, $params = '')
    {
        try {
            return self::callStatic($command, $params);
        } catch (\Exception $e) {
            try {
                return self::callStatic('help', $params);
            } catch (\Exception $e) {
                return false;
            }
        }
    }

    private static function callStatic($command, $params)
    {
        return forward_static_call(
            [Bot::class, $command],
            $params
        );
    }

    private static function formatResponse($response)
    {
        $text = $response;
        if (is_array($response)) {
            $text = '';
            foreach ($response as $to) {
                foreach ($to as $toName => $toValue) {
                    $text .= $toName . ' ' . $toValue . PHP_EOL;
                }
            }
        }
        return $text;
    }
}
