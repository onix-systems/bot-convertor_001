<?php

namespace App\Http\Controllers;

use App\Facades\Bot;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class SlackController extends Controller
{
    public function main(Request $request)
    {
        $text = $request->input('text');
        $channelId = $request->input('channel_id');
        $userId = $request->input('user_id');

        if ($userId !== config('slack.bot_id')) {
            if (starts_with($text, '_')) {
                $parts = explode(' ', ltrim($text, '_'));
                $command = $parts[0];
                $args = implode(' ', array_slice($parts, 1));
                self::sendResponse(
                    self::callBotCommand(mb_strtolower($command), $args),
                    $channelId
                );
            } else {
                self::sendResponse(
                    self::formatResponse(Bot::convert($text)),
                    $channelId
                );
            }
        }
    }

    private static function sendResponse($text, $channelId)
    {
        (new Client)
            ->get(
                "https://slack.com/api/chat.postMessage", [
                    'query' => [
                        'token' => config('slack.token'),
                        'channel' => $channelId,
                        'text' => $text,
                        'username' => 'converterbot',
                        'as_user' => true
                    ]
                ]
            );
    }

    private static function callBotCommand($command, $params = '')
    {
        try {
            return self::callStatic($command, $params);
        } catch (\Exception $e) {
            try {
                return self::callStatic('help', $params);
            } catch (\Exception $e) {
                return false;
            }
        }
    }

    private static function callStatic($command, $params)
    {
        return forward_static_call(
            [Bot::class, $command],
            $params
        );
    }

    private static function formatResponse($response)
    {
        $text = $response;
        if (is_array($response)) {
            $text = '';
            foreach ($response as $to) {
                foreach ($to as $toName => $toValue) {
                    $text .= $toName . ' ' . $toValue . PHP_EOL;
                }
            }
        }
        return $text;
    }
}
