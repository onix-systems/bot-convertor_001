<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Telegram
Route::group(['prefix' => config('telegram.bot_token')], function () {

    Route::post('/', 'TelegramController@main');

});

// Facebook
Route::post('/webhook/', 'FacebookController@main');
Route::get('/webhook/', 'FacebookController@verify');

// Skype
Route::get('/skype/', 'SkypeController@main');
Route::post('/skype/', 'SkypeController@main');

// Slack
Route::post('/slack/', 'SlackController@main');