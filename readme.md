# Telegram Converter Bot
### Describing of the development process
For development of this code **use** a **dev** branch.

### Installation
```shell
$ git clone git@bitbucket.org:onix-systems/bot-convertor_001.git

$ cd bot-convertor_001

$ composer install
```

### Launching it in the docker container
```shell
$ git clone git@bitbucket.org:onix-systems/bot-convertor_001.git

$ cd bot-convertor_001

$ docker build -t bot-convertor_001 ./

$ docker run -d -h bot-convertor_001 --name bot-convertor_001 -p 80:80 --restart=always --log-driver=json-file --log-opt max-size=100m --log-opt max-file=5 bot-convertor_001

```

#### Reinitialization of the bot in the docker container
```shell
$ docker stop bot-convertor_001

$ docker rm bot-convertor_001

$ docker run -d -h bot-convertor_001 --name bot-convertor_001 -p 80:80 --restart=always --log-driver=json-file --log-opt max-size=100m --log-opt max-file=5 bot-convertor_001

```