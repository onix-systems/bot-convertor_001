FROM ubuntu:trusty

ENV PROJECT_FOLDER="/var/www/project"

RUN sed -i "s/archive/ua.archive/g" /etc/apt/sources.list

RUN apt-get update

RUN apt-get install -y \
	apache2 \
	php5 \
	wget \
	supervisor
	
# wget package is used for updating a cerificate database in this image
# without updating of the certificate database there will be a SSL error on the step, when is launched php compose-setup.php

# Installing composer 

WORKDIR /tmp

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php -r "if (hash_file('SHA384', 'composer-setup.php') === '92102166af5abdb03f49ce52a40591073a7b859a86e8ff13338cf7db58a19f7844fbc0bb79b2773bf30791e935dbd938') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
RUN php composer-setup.php --install-dir=/usr/local/bin --filename=composer
RUN php -r "unlink('composer-setup.php');"

RUN mkdir -p ${PROJECT_FOLDER}

WORKDIR ${PROJECT_FOLDER}

# Copying the project inside the image
COPY ./ ./

RUN composer install

# TODO: Remove this section after debug
RUN apt-get install -y mc

# Configuring the apache

RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf
# Allow access through the apache into project folder
RUN echo "\
<Directory ${PROJECT_FOLDER}>\n\
    Options Indexes FollowSymLinks\n\
    AllowOverride All\n\
    Require all granted\n\
</Directory>\
" > /etc/apache2/conf-enabled/project.conf

RUN sed -i -r "s/\/var\/www\/html/\/var\/www\/project\/public/" /etc/apache2/sites-enabled/000-default.conf

RUN a2enmod rewrite

RUN chown -R www-data.www-data ${PROJECT_FOLDER}

RUN ln -sf /dev/stdout /var/log/apache2/access.log
RUN ln -sf /dev/stderr /var/log/apache2/error.log

#
# Configuring a supervisord service to run only apache
#
RUN echo "\
[supervisord]\n\
nodaemon=true\n\
loglevel=critical\n\
logfile_maxbytes=10240\n\
[program:apache2]\n\
command=apachectl -D \"FOREGROUND\" -k start\n\
stdout_logfile=/dev/stdout\n\
stdout_logfile_maxbytes=0\n\
stderr_logfile=/dev/stderr\n\
stderr_logfile_maxbytes=0\n\
" > /etc/supervisor/conf.d/supervisord.conf
#

EXPOSE 80

CMD ["/usr/bin/supervisord"]
